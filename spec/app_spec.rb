require "./app"
require File.dirname(__FILE__) + '/spec_helper'

def app
  Sinatra::Application
end

describe "Main Page" do
  include Rack::Test::Methods
  it "should load the main page" do
    get '/'
    last_response.body.should == "Hello world"
  end
end
