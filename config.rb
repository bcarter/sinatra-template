require "sinatra"
require "haml"
require "less"
require "coffee-script"

$LOAD_PATH.unshift("#{File.dirname(__FILE__)}/lib")
Dir.glob("#{File.dirname(__FILE__)}/lib/*.rb") do |lib|
  require File.basename(lib, '.*')
end

helpers do
  def partial(page, options={})
    haml page, options.merge!(:layout => false)
  end
end

get '/*.css' do
  content_type 'text/css', :charset => 'utf-8' 
  filename = params[:splat].first
  scss filename.to_sym
end

get '/*.js' do
  filename = params[:splat].first 
  coffee filename.to_sym
end
